﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IWeaponService
    {
        /// <summary>
        /// Retrieves a players weapon from database
        /// </summary>
        /// <returns></returns>
        Task<WeaponDto> GetPlayerWeaponAsync(int userId);

        /// <summary>
        /// updates a players weapon
        /// </summary>
        /// <returns></returns>
        Task<WeaponDto> UpdatePlayerWeaponAsync(int userId, WeaponDto weapon);
    }
}
