﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IWeaponRarityService
    {
        /// <summary>
        /// Retrieves all weaponrarities from database
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<WeaponRarityDto>> GetAllAsync();
    }
}
