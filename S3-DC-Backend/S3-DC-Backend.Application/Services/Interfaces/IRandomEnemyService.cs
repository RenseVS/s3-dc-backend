﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IRandomEnemyService
    {
        /// <summary>
        /// Gets a random generated enemy
        /// </summary>
        /// <returns>A random Enemy or null in case of failure</returns>
        Task<EnemyDto> GetRandomEnemyAsync();
    }
}
