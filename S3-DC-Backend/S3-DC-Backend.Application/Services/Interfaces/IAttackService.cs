﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IAttackService
    {
        /// <summary>
        /// Retrieves a players Enemy from database
        /// </summary>
        /// <returns></returns>
        Task<UserDto> AttackPlayerEnemyAsync(int userId);
    }
}
