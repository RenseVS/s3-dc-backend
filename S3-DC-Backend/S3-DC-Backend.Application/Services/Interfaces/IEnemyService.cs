﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IEnemyService
    {
        /// <summary>
        /// Retrieves a players Enemy from database
        /// </summary>
        /// <returns></returns>
        Task<EnemyDto> GetPlayerEnemyAsync(int userId);

        /// <summary>
        /// updates a players Enemy
        /// </summary>
        /// <returns></returns>
        Task<EnemyDto> UpdatePlayerEnemyAsync(int userId, EnemyDto enemy);
    }
}
