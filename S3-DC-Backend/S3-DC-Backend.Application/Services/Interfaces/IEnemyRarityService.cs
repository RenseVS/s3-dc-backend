﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IEnemyRarityService
    {
        /// <summary>
        /// Retrieves all enemy rarities from database
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EnemyRarityDto>> GetAllAsync();
    }
}
