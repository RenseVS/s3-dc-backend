﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IRandomWeaponService
    {
        /// <summary>
        /// Gets a random generated weapon
        /// </summary>
        /// <returns>A random weapon or null in case of failure</returns>
        Task<WeaponDto> GetRandomWeaponAsync();
    }
}
