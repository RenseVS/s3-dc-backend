﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;

namespace S3_DC_Backend.Application.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Retrieves a user from database by id
        /// </summary>
        /// <returns></returns>
        Task<UserDto> GetById(int userId);
        /// <summary>
        /// Retrieves a user from database by id
        /// </summary>
        /// <returns></returns>
        Task<UserDto> GetByIdWithWeaponAndEnemy(int userId);
        /// <summary>
        /// Updates user in db
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<UserDto> UpdateUser(UserDto user);
    }
}
