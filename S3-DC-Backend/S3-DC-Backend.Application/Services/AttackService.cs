﻿using System;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class AttackService : IAttackService
    {
        public readonly IUserService _userService;
        public readonly IEnemyService _enemyService;
        public AttackService(IUserService userService, IEnemyService enemyService)
        {
            _userService = userService;
            _enemyService = enemyService;
        }

        public async Task<UserDto> AttackPlayerEnemyAsync(int userId)
        {
            var user = await _userService.GetByIdWithWeaponAndEnemy(userId);
            if (user == null)
            {
                return user;
            }
            int userspeed = user.Weapon.BaseSpeed * user.Weapon.WeaponRarity.SpeedModifier;
            int enemyspeed = user.Enemy.BaseSpeed * user.Enemy.EnemyRarity.SpeedModifier;
            if(userspeed >= enemyspeed)
            {
                UserAttack(user);
                if (IsDead(user.Enemy.Health))
                {
                    user.Enemy.Health = 0;
                }
                else
                {
                    EnemyAttack(user);
                    if (IsDead(user.Enemy.Health))
                    {
                        user.Enemy.Health = 0;
                    }
                    await _userService.UpdateUser(user);
                }
                await _enemyService.UpdatePlayerEnemyAsync(user.Id, user.Enemy);
            }
            else {
                EnemyAttack(user);
                if (IsDead(user.Health))
                {
                    user.Health = 0;
                }
                else
                {
                    UserAttack(user);
                    if (IsDead(user.Health))
                    {
                        user.Health = 0;
                    }
                    await _enemyService.UpdatePlayerEnemyAsync(user.Id, user.Enemy);
                }
                await _userService.UpdateUser(user);
            }
            return user;
        }
        private UserDto UserAttack(UserDto user)
        {
            user.Enemy.Health -= user.Weapon.BaseDamage * user.Weapon.WeaponRarity.DamageModifier;
            return user;
        }
        private UserDto EnemyAttack(UserDto user)
        {
            user.Health -= user.Enemy.BaseDamage * user.Enemy.EnemyRarity.DamageModifier;
            return user;
        }
        private bool IsDead(int health)
        {
            if(health <= 0)
            {
                return true;
            }
            return false;
        }
    }
}
