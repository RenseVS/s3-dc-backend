﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class WeaponTemplateService : IWeaponTemplateService
    {
        readonly IAppDbContext _dbContext;
        public WeaponTemplateService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<WeaponTemplateDto>> GetAllAsync()
        {
            return await _dbContext.WeaponTemplates
                .Select(weaponTemplate => new WeaponTemplateDto(weaponTemplate))
                .ToListAsync();
        }
    }
}
