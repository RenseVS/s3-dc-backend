﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Services
{
    public class EnemyService : IEnemyService
    {
        readonly IAppDbContext _dbContext;
        public EnemyService(IAppDbContext appDbContext)
        {
            _dbContext = appDbContext;
        }

        public async Task<EnemyDto> GetPlayerEnemyAsync(int userId)
        {
            var enemy = await _dbContext.Enemies
                .Where(u => u.UserId == userId)
                .Include(r => r.EnemyRarity)
                .FirstOrDefaultAsync();
            return enemy != null ? new EnemyDto(enemy) : null;
        }

        public async Task<EnemyDto> UpdatePlayerEnemyAsync(int userId, EnemyDto enemy)
        {
            var currentenemy = await _dbContext.Enemies
                .Where(u => u.UserId == userId)
                .FirstOrDefaultAsync();
            if (currentenemy != null)
            {
                currentenemy.BaseDamage = enemy.BaseDamage;
                currentenemy.BaseSpeed = enemy.BaseSpeed;
                currentenemy.EnemyIconPath = enemy.EnemyIconPath;
                currentenemy.EnemyRarityId = enemy.EnemyRarity.Id;
                currentenemy.Health = enemy.Health;
                currentenemy.MaxHealth = enemy.MaxHealth;
                currentenemy.Name = enemy.Name;
                currentenemy.WeaponIconPath = enemy.WeaponIconPath;
                currentenemy.UserId = userId;
                await _dbContext.SaveChangesAsync();
                return new EnemyDto(currentenemy);
            }
            else
            {
                var newEnemy = new Enemy()
                {
                    BaseDamage = enemy.BaseDamage,
                    BaseSpeed = enemy.BaseSpeed,
                    EnemyIconPath = enemy.EnemyIconPath,
                    EnemyRarityId = enemy.EnemyRarity.Id,
                    Health = enemy.Health,
                    MaxHealth = enemy.MaxHealth,
                    Name = enemy.Name,
                    WeaponIconPath = enemy.WeaponIconPath,
                    UserId = userId,
                };
                _dbContext.Enemies.Add(newEnemy);
                await _dbContext.SaveChangesAsync();
                return new EnemyDto(newEnemy);
            }
        }
    }
}
