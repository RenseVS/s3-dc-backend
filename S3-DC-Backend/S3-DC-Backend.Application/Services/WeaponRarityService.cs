﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class WeaponRarityService : IWeaponRarityService
    {
        readonly IAppDbContext _dbContext;
        public WeaponRarityService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<WeaponRarityDto>> GetAllAsync()
        {
            return await _dbContext.WeaponRarities
                .Select(weaponRarity => new WeaponRarityDto(weaponRarity))
                .ToListAsync();
        }
    }
}
