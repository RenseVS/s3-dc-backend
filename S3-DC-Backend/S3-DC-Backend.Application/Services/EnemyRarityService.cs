﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class EnemyRarityService : IEnemyRarityService
    {
        readonly IAppDbContext _dbContext;
        public EnemyRarityService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<EnemyRarityDto>> GetAllAsync()
        {
            return await _dbContext.EnemyRarities
                .Select(enemyRarity => new EnemyRarityDto(enemyRarity))
                .ToListAsync();
        }
    }
}
