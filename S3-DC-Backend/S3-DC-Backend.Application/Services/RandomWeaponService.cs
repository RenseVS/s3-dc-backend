﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class RandomWeaponService : IRandomWeaponService
    {
        readonly IAppDbContext _context;
        readonly IWeaponTemplateService _weaponTemplateService;
        readonly IWeaponRarityService _weaponRarityService;
        public RandomWeaponService(IAppDbContext context, IWeaponTemplateService weaponTemplateService, IWeaponRarityService weaponRarityService)
        {
            _context = context;
            _weaponTemplateService = weaponTemplateService;
            _weaponRarityService = weaponRarityService;
        }

        public async Task<WeaponDto> GetRandomWeaponAsync()
        {
            var weapon = new WeaponDto(await GetRandomWeaponTemplateAsync());
            var rarity = await GetRandomRarityAsync();
            if (weapon == null || rarity == null)
            {
                return new WeaponDto();
            }
            weapon.WeaponRarity = rarity;
            return weapon;
        }

        private async Task<WeaponTemplateDto> GetRandomWeaponTemplateAsync()
        {
            var result = await _weaponTemplateService.GetAllAsync();
            List<WeaponTemplateDto> weaponTemplates = result.ToList();
            var random = new Random();
            int index = random.Next(0, weaponTemplates.Count());
            return weaponTemplates[index];
        }

        private async Task<WeaponRarityDto> GetRandomRarityAsync()
        {
            var result = await _weaponRarityService.GetAllAsync();
            List<WeaponRarityDto> weaponRarities = result.ToList();
            if (!RarityValidator(weaponRarities))
            {
                return new WeaponRarityDto();
            }
            var random = new Random();
            int index = random.Next(0, 101);
            var chances = RarityChances(weaponRarities);
            for (int i = 0; i < chances.Count(); i++)
            {
                if (chances[i] >= index)
                {
                    return weaponRarities[i];
                }
            }
            return new WeaponRarityDto();
        }

        private bool RarityValidator(List<WeaponRarityDto> rarities)
        {
            int TotalRarityOdds = 0;
            foreach (WeaponRarityDto rarity in rarities)
            {
                TotalRarityOdds += rarity.AppearChance;
            }
            if (TotalRarityOdds == 100)
            {
                return true;
            }
            return false;
        }

        private List<int> RarityChances(List<WeaponRarityDto> _rarities)
        {
            var rarities = _rarities;
            rarities = rarities.OrderBy(o => o.AppearChance).ToList();
            var Chances = new List<int>();
            for (int i = 0; i < rarities.Count(); i++)
            {
                if (i == 0)
                {
                    Chances.Add(rarities[0].AppearChance);
                }
                else
                {
                    Chances.Add(Chances[i - 1] + rarities[i].AppearChance);
                }
            }
            return Chances;
        }
    }
}
