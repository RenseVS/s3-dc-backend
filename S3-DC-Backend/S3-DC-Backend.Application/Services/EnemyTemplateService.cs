﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class EnemyTemplateService : IEnemyTemplateService
    {
        readonly IAppDbContext _dbContext;
        public EnemyTemplateService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<EnemyTemplateDto>> GetAllAsync()
        {
            return await _dbContext.EnemyTemplates
                .Select(enemyTemplate => new EnemyTemplateDto(enemyTemplate))
                .ToListAsync();
        }
    }
}
