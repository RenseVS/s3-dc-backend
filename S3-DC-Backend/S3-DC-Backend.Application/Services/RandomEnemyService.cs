﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class RandomEnemyService : IRandomEnemyService
    {
        readonly IAppDbContext _context;
        readonly IEnemyRarityService _enemyRarityService;
        readonly IEnemyTemplateService _enemyTemplateService;
        public RandomEnemyService(IAppDbContext context, IEnemyRarityService enemyRarityService, IEnemyTemplateService enemyTemplateService)
        {
            _context = context;
            _enemyRarityService = enemyRarityService;
            _enemyTemplateService = enemyTemplateService;
        }

        public async Task<EnemyDto> GetRandomEnemyAsync()
        {
            var enemy = new EnemyDto(await GetRandomEnemyTemplateAsync());
            var rarity = await GetRandomRarityAsync();
            if (enemy == null || rarity == null)
            {
                return new EnemyDto();
            }
            enemy.EnemyRarity = rarity;
            enemy.MaxHealth *= rarity.HealthModifier;
            enemy.Health = enemy.MaxHealth;
            return enemy;
        }

        private async Task<EnemyTemplateDto> GetRandomEnemyTemplateAsync()
        {
            var result = await _enemyTemplateService.GetAllAsync();
            List<EnemyTemplateDto> enemyTemplates = result.ToList();
            var random = new Random();
            int index = random.Next(0, enemyTemplates.Count());
            return enemyTemplates[index];
        }

        private async Task<EnemyRarityDto> GetRandomRarityAsync()
        {
            var result = await _enemyRarityService.GetAllAsync();
            List<EnemyRarityDto> enemyRarities = result.ToList();
            if (!RarityValidator(enemyRarities))
            {
                return new EnemyRarityDto();
            }
            var random = new Random();
            int index = random.Next(0, 101);
            var chances = RarityChances(enemyRarities);
            for (int i = 0; i < chances.Count(); i++)
            {
                if (chances[i] >= index)
                {
                    return enemyRarities[i];
                }
            }
            return new EnemyRarityDto();
        }


        private bool RarityValidator(List<EnemyRarityDto> rarities)
        {
            int TotalRarityOdds = 0;
            foreach (EnemyRarityDto rarity in rarities)
            {
                TotalRarityOdds += rarity.AppearChance;
            }
            if (TotalRarityOdds == 100)
            {
                return true;
            }
            return false;
        }

        private List<int> RarityChances(List<EnemyRarityDto> _rarities)
        {
            var rarities = _rarities;
            rarities = rarities.OrderBy(o => o.AppearChance).ToList();
            var Chances = new List<int>();
            for (int i = 0; i < rarities.Count(); i++)
            {
                if (i == 0)
                {
                    Chances.Add(rarities[0].AppearChance);
                }
                else
                {
                    Chances.Add(Chances[i - 1] + rarities[i].AppearChance);
                }
            }
            return Chances;
        }
    }
}
