﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class WeaponService : IWeaponService
    {
        readonly IAppDbContext _dbContext;
        public WeaponService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<WeaponDto> GetPlayerWeaponAsync(int userId)
        {
            var weapon = await _dbContext.Weapons
                .Where(u => u.UserId == userId)
                .Include(r => r.WeaponRarity)
                .FirstOrDefaultAsync();
            return weapon != null ? new WeaponDto(weapon) : null;
        }

        public async Task<WeaponDto> UpdatePlayerWeaponAsync(int userId, WeaponDto weapon)
        {
            var currentweapon = await _dbContext.Weapons
                .Where(u => u.UserId == userId)
                .FirstOrDefaultAsync();
            if (currentweapon != null)
            {
                currentweapon.WeaponRarityId = weapon.WeaponRarity.Id;
                currentweapon.BaseDamage = weapon.BaseDamage;
                currentweapon.BaseSpeed = weapon.BaseSpeed;
                currentweapon.Name = weapon.Name;
                currentweapon.WeaponIconPath = weapon.WeaponIconPath;
                await _dbContext.SaveChangesAsync();
                return new WeaponDto(currentweapon);
            }
            return null;
        }
    }
}
