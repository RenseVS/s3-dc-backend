﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

namespace S3_DC_Backend.Application.Services
{
    public class UserService : IUserService
    {
        readonly IAppDbContext _dbContext;
        public UserService(IAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<UserDto> GetById(int userId)
        {
            var user = await _dbContext.Users
                .Where(u => u.Id == userId)
                .FirstOrDefaultAsync();
            return user != null ? new UserDto(user) : null;
        }

        public async Task<UserDto> GetByIdWithWeaponAndEnemy(int userId)
        {
            var user = await _dbContext.Users
               .Where(u => u.Id == userId)
               .Include(w => w.Weapon)
               .Include(wr => wr.Weapon.WeaponRarity)
               .Include(e => e.Enemy)
               .Include(er => er.Enemy.EnemyRarity)
               .FirstOrDefaultAsync();
            return user != null ? new UserDto(user) : null;
        }

        public async Task<UserDto> UpdateUser(UserDto user)
        {
            var currentuser = await _dbContext.Users
                .Where(u => u.Id == user.Id)
                .FirstOrDefaultAsync();
            if (currentuser != null)
            {
                currentuser.Health = user.Health;
                currentuser.Name = user.Name;
                await _dbContext.SaveChangesAsync();
                return new UserDto(currentuser);
            }
            return null;
        }
    }
}
