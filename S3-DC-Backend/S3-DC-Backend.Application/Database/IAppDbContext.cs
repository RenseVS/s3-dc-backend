﻿using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Domain.Classes;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Application.Database
{
    public interface IAppDbContext : IDbContext
    {
        DbSet<Weapon> Weapons { get; }

        DbSet<WeaponTemplate> WeaponTemplates { get; }

        DbSet<Enemy> Enemies { get; }

        DbSet<EnemyTemplate> EnemyTemplates { get; }

        DbSet<WeaponRarity> WeaponRarities { get; }

        DbSet<User> Users { get; }

        DbSet<EnemyRarity> EnemyRarities { get; }
    }
}
