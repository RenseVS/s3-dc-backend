﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace S3_DC_Backend.Application.Database
{
    public interface IDbContext : IDisposable
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
