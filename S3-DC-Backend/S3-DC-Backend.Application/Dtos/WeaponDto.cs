﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data to the presentation layer.
    /// </summary>
    [Serializable]
    public class WeaponDto : WeaponDescriptionDto
    {
        public WeaponDto() { }

        /// <summary>
        /// Constructs an `WeaponDto` from an `Weapon` object.
        /// </summary>
        /// <param name="weapon"></param>
        public WeaponDto(Weapon weapon)
            : base(weapon)
        {
            WeaponRarity = weapon.WeaponRarity != null
                ? new WeaponRarityDescriptionDto(weapon.WeaponRarity)
                : null;
        }

        public WeaponDto(WeaponTemplateDto weaponTemplate)
            : base(weaponTemplate) { }

        [JsonPropertyName("weapon_rarity")]
        public WeaponRarityDescriptionDto WeaponRarity { get; set; }
    }
}
