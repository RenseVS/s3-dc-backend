﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    public class EnemyTemplateDescriptionDto
    {
        public EnemyTemplateDescriptionDto() { }

        /// <summary>
        /// Constructs an `EnemyTemplateDescriptionDto` from an `EnemyTemplate`.
        /// </summary>
        /// <param name="weapontemplate"></param>
        public EnemyTemplateDescriptionDto(EnemyTemplate enemyTemplate)
        {
            Id = enemyTemplate.Id;
            Name = enemyTemplate.Name;
            WeaponIconPath = enemyTemplate.WeaponIconPath;
            BaseSpeed = enemyTemplate.BaseSpeed;
            BaseDamage = enemyTemplate.BaseDamage;
            EnemyIconPath = enemyTemplate.EnemyIconPath;
            MaxHealth = enemyTemplate.MaxHealth;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("max_health")]
        public int MaxHealth { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("weapon_icon_path")]
        public string WeaponIconPath { get; set; }

        [JsonPropertyName("enemy_icon_path")]
        public string EnemyIconPath { get; set; }

        [JsonPropertyName("base_speed")]
        public int BaseSpeed { get; set; }

        [JsonPropertyName("base_damage")]
        public int BaseDamage { get; set; }
    }
}
