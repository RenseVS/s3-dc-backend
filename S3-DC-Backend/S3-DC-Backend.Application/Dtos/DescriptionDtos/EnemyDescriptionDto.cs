﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data without circular references to the presentation layer.
    /// </summary>
    public class EnemyDescriptionDto
    {
        public EnemyDescriptionDto() { }

        /// <summary>
        /// Constructs an `EnemyDescriptionDto` from an `Enemy`.
        /// </summary>
        /// <param name="enemy"></param>
        public EnemyDescriptionDto(Enemy enemy)
        {
            Id = enemy.Id;
            Name = enemy.Name;
            WeaponIconPath = enemy.WeaponIconPath;
            BaseSpeed = enemy.BaseSpeed;
            BaseDamage = enemy.BaseDamage;
            EnemyIconPath = enemy.EnemyIconPath;
            Health = enemy.Health;
            MaxHealth = enemy.MaxHealth;
        }

        /// <summary>
        /// Constructs an `EnemyDescriptionDto` from an `EnemyTemplate`.
        /// </summary>
        /// <param name="enemytemplate"></param>
        public EnemyDescriptionDto(EnemyTemplateDto enemyTemplate)
        {
            Id = enemyTemplate.Id;
            Name = enemyTemplate.Name;
            WeaponIconPath = enemyTemplate.WeaponIconPath;
            BaseSpeed = enemyTemplate.BaseSpeed;
            BaseDamage = enemyTemplate.BaseDamage;
            EnemyIconPath = enemyTemplate.EnemyIconPath;
            Health = 0;
            MaxHealth = enemyTemplate.MaxHealth;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("health")]
        public int Health { get; set; }

        [JsonPropertyName("max_health")]
        public int MaxHealth { get; set; }

        [JsonPropertyName("weapon_icon_Path")]
        public string WeaponIconPath { get; set; }

        [JsonPropertyName("enemy_icon_Path")]
        public string EnemyIconPath { get; set; }

        [JsonPropertyName("base_speed")]
        public int BaseSpeed { get; set; }

        [JsonPropertyName("base_damage")]
        public int BaseDamage { get; set; }
    }
}
