﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data without circular references to the presentation layer.
    /// </summary>
    public class WeaponDescriptionDto
    {
        public WeaponDescriptionDto() { }

        /// <summary>
        /// Constructs an `WeaponDescriptionDto` from an `Weapon`.
        /// </summary>
        /// <param name="weapon"></param>
        public WeaponDescriptionDto(Weapon weapon)
        {
            Id = weapon.Id;
            Name = weapon.Name;
            WeaponIconPath = weapon.WeaponIconPath;
            BaseSpeed = weapon.BaseSpeed;
            BaseDamage = weapon.BaseDamage;
        }

        /// <summary>
        /// Constructs an `WeaponDescriptionDto` from an `WeaponTemplate`.
        /// </summary>
        /// <param name="weapontemplate"></param>
        public WeaponDescriptionDto(WeaponTemplateDto weapon)
        {
            Id = weapon.Id;
            Name = weapon.Name;
            WeaponIconPath = weapon.WeaponIconPath;
            BaseSpeed = weapon.BaseSpeed;
            BaseDamage = weapon.BaseDamage;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("weapon_icon_path")]
        public string WeaponIconPath { get; set; }

        [JsonPropertyName("base_speed")]
        public int BaseSpeed { get; set; }

        [JsonPropertyName("base_damage")]
        public int BaseDamage { get; set; }
    }
}
