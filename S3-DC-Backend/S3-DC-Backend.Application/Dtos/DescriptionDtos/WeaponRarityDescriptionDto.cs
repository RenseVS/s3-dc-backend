﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data without circular references to the presentation layer.
    /// </summary>
    public class WeaponRarityDescriptionDto
    {
        public WeaponRarityDescriptionDto() { }

        /// <summary>
        /// Constructs an `WeaponRarityDescriptionDto` from an `WeaponRarity`.
        /// </summary>
        /// <param name="weaponRarity"></param>
        public WeaponRarityDescriptionDto(WeaponRarity weaponRarity)
        {
            Id = weaponRarity.Id;
            Rarity = weaponRarity.Rarity;
            RarityColor = weaponRarity.RarityColor;
            DamageModifier = weaponRarity.DamageModifier;
            SpeedModifier = weaponRarity.SpeedModifier;
            AppearChance = weaponRarity.AppearChance;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("rarity")]
        public string Rarity { get; set; }

        [JsonPropertyName("rarity_color")]
        public string RarityColor { get; set; }

        [JsonPropertyName("damage_modifier")]
        public int DamageModifier { get; set; }

        [JsonPropertyName("speed_modifier")]
        public int SpeedModifier { get; set; }

        [JsonPropertyName("appear_chance")]
        public int AppearChance { get; set; }
    }
}
