﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    public class EnemyRarityDescriptionDto
    {
        public EnemyRarityDescriptionDto() { }
        /// <summary>
        /// Constructs an `EnemyRarityDescriptionDto` from an `WeaponRarity`.
        /// </summary>
        /// <param name="weaponRarity"></param>
        public EnemyRarityDescriptionDto(EnemyRarity enemyRarity)
        {
            Id = enemyRarity.Id;
            Rarity = enemyRarity.Rarity;
            RarityColor = enemyRarity.RarityColor;
            DamageModifier = enemyRarity.DamageModifier;
            SpeedModifier = enemyRarity.SpeedModifier;
            AppearChance = enemyRarity.AppearChance;
            HealthModifier = enemyRarity.HealthModifier;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("rarity")]
        public string Rarity { get; set; }

        [JsonPropertyName("rarity_color")]
        public string RarityColor { get; set; }

        [JsonPropertyName("damage_modifier")]
        public int DamageModifier { get; set; }

        [JsonPropertyName("speed_modifier")]
        public int SpeedModifier { get; set; }

        [JsonPropertyName("appear_chance")]
        public int AppearChance { get; set; }

        [JsonPropertyName("health_modifier")]
        public int HealthModifier { get; set; }
    }
}
