﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    public class UserDescriptionDto
    {
        public UserDescriptionDto() { }

        public UserDescriptionDto(User user)
        {
            Id = user.Id;
            Name = user.Name;
            Health = user.Health;
            MaxHealth = user.MaxHealth;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("health")]
        public int Health { get; set; }

        [JsonPropertyName("max_health")]
        public int MaxHealth { get; set; }
    }
}
