﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Domain.Classes;

namespace S3_DC_Backend.Application.Dtos.DescriptionDtos
{
    public class WeaponTemplateDescriptionDto
    {
        public WeaponTemplateDescriptionDto() { }

        /// <summary>
        /// Constructs an `WeaponTemplateDescriptionDto` from an `WeaponTemplate`.
        /// </summary>
        /// <param name="weapontemplate"></param>
        public WeaponTemplateDescriptionDto(WeaponTemplate weaponTemplate)
        {
            Id = weaponTemplate.Id;
            Name = weaponTemplate.Name;
            WeaponIconPath = weaponTemplate.WeaponIconPath;
            BaseSpeed = weaponTemplate.BaseSpeed;
            BaseDamage = weaponTemplate.BaseDamage;
        }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("weapon_icon_path")]
        public string WeaponIconPath { get; set; }

        [JsonPropertyName("base_speed")]
        public int BaseSpeed { get; set; }

        [JsonPropertyName("base_damage")]
        public int BaseDamage { get; set; }
    }
}
