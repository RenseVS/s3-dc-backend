﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos
{
    public class UserDto : UserDescriptionDto
    {
        public UserDto() { }

        public UserDto(User user)
            : base(user)
        {
            if (user.Weapon != null)
            {
                Weapon = new WeaponDto(user.Weapon);
            }
            if (user.Enemy != null)
            {
                Enemy = new EnemyDto(user.Enemy);
            }
        }

        [JsonPropertyName("weapon")]
        public WeaponDto Weapon { get; set; }

        [JsonPropertyName("enemy")]
        public EnemyDto Enemy { get; set; }

    }
}
