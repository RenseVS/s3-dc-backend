﻿using System;
using System.Text.Json.Serialization;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes.GameObjects;

namespace S3_DC_Backend.Application.Dtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data to the presentation layer.
    /// </summary>
    [Serializable]
    public class EnemyDto : EnemyDescriptionDto
    {
        public EnemyDto() { }

        /// <summary>
        /// Constructs an `WeaponDto` from an `Weapon` object.
        /// </summary>
        /// <param name="weapon"></param>
        public EnemyDto(Enemy enemy)
            : base(enemy)
        {
            if(enemy.EnemyRarity != null)
            {
                EnemyRarity = new EnemyRarityDto(enemy.EnemyRarity);
            }
        }

        public EnemyDto(EnemyTemplateDto enemyTemplate)
            : base(enemyTemplate) {
        }

        [JsonPropertyName("enemy_rarity")]
        public EnemyRarityDescriptionDto EnemyRarity { get; set; }
    }
}
