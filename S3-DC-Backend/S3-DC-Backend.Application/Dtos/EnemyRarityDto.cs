﻿using System;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Application.Dtos
{
    public class EnemyRarityDto : EnemyRarityDescriptionDto
    {
        public EnemyRarityDto() { }

        /// <summary>
        /// Constructs a `EnemyRarityDto` from an `EnemyRarity` object.
        /// </summary>
        /// <param name="weaponRarity"></param>
        public EnemyRarityDto(EnemyRarity enemyRarity)
            : base(enemyRarity)
        { }
    }
}
