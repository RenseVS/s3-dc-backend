﻿using System;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Application.Dtos
{
    /// <summary>
    /// An object that is used to safely transfer assignment data without circular references to the presentation layer.
    /// </summary>
    public class WeaponRarityDto : WeaponRarityDescriptionDto
    {
        public WeaponRarityDto() { }
        /// <summary>
        /// Constructs a `WeaponRarityDto` from an `WeaponRarity` object.
        /// </summary>
        /// <param name="weaponRarity"></param>
        public WeaponRarityDto(WeaponRarity weaponRarity)
            : base(weaponRarity)
        { }
    }
}
