﻿using System;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Domain.Classes;

namespace S3_DC_Backend.Application.Dtos
{
    /// <summary>
    /// An object that is used to safely transfer exercise data to the presentation layer.
    /// </summary>
    public class EnemyTemplateDto : EnemyTemplateDescriptionDto
    {
        public EnemyTemplateDto() { }

        public EnemyTemplateDto(EnemyTemplate enemyTemplate)
            : base(enemyTemplate)
        { }
    }
}
