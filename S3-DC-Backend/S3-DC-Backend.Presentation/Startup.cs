using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Application.Services.Interfaces;
using S3_DC_Backend.Persistence.Database;
using Microsoft.OpenApi.Models;

namespace S3_DC_Backend.Presentation
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        bool _isTestingEnv = false;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _isTestingEnv = env.IsEnvironment("Testing");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (!_isTestingEnv)
            {
                services.AddDbContext<IAppDbContext, AppDbContext>(options =>
                options.UseMySql(Configuration["Connectionstring:myDb"],
                        mySqlOptions => mySqlOptions.CharSetBehavior(CharSetBehavior.NeverAppend)
                    )
                );
            }
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "s3-project API v1", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("*")
                                                          .AllowAnyHeader()
                                                          .AllowAnyMethod();
                                  });
            });

            #region Services 
            services.AddTransient<IRandomWeaponService, RandomWeaponService>();
            services.AddTransient<IRandomEnemyService, RandomEnemyService>();
            services.AddTransient<IWeaponRarityService, WeaponRarityService>();
            services.AddTransient<IWeaponTemplateService, WeaponTemplateService>();
            services.AddTransient<IEnemyRarityService, EnemyRarityService>();
            services.AddTransient<IEnemyTemplateService, EnemyTemplateService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IWeaponService, WeaponService>();
            services.AddTransient<IEnemyService, EnemyService>();
            services.AddTransient<IAttackService, AttackService>();
            #endregion
            services.AddControllers(options =>
            {
                options.SuppressAsyncSuffixInActionNames = false;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "s3-project API v1"));
            }

            app.UseCors(MyAllowSpecificOrigins);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
