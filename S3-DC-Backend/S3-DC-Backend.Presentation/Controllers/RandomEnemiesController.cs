﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace S3_DC_Backend.Presentation.Controllers
{
    [Route("api/randomenemies")]
    [ApiController]
    public class RandomEnemiesController : ControllerBase
    {
        readonly IRandomEnemyService _randomEnemyService;
        public RandomEnemiesController(IRandomEnemyService randomEnemyService)
        {
            _randomEnemyService = randomEnemyService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(EnemyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<EnemyDto>> GetAsync()
        {
            var enemy = await _randomEnemyService.GetRandomEnemyAsync();
            if (enemy == null)
            {
                return NoContent();
            }

            return Ok(enemy);
        }
    }
}
