﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace S3_DC_Backend.Presentation.Controllers
{
    [Route("api/enemy")]
    [ApiController]
    public class EnemiesController : Controller
    {
        readonly IEnemyService _enemyService;
        public EnemiesController(IEnemyService enemyService)
        {
            _enemyService = enemyService;
        }

        [HttpGet("{userid}")]
        [ProducesResponseType(typeof(EnemyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EnemyDto>> GetAsync(int userid)
        {
            var enemy = await _enemyService.GetPlayerEnemyAsync(userid);
            if (enemy == null)
            {
                return NoContent();
            }
            return Ok(enemy);
        }

        [HttpPut("{userid}")]
        [ProducesResponseType(typeof(EnemyDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EnemyDto>> Put(int userid, [FromBody] EnemyDto enemy)
        {
            var result = await _enemyService.UpdatePlayerEnemyAsync(userid, enemy);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(enemy);
        }
    }
}
