﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace S3_DC_Backend.Presentation.Controllers
{
    [Route("api/Weapon")]
    [ApiController]
    public class WeaponsController : Controller
    {
        readonly IWeaponService _weaponService;
        public WeaponsController(IWeaponService weaponService)
        {
            _weaponService = weaponService;
        }

        [HttpGet("{userid}")]
        [ProducesResponseType(typeof(WeaponDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<WeaponDto>> GetAsync(int userid)
        {
            var weapon = await _weaponService.GetPlayerWeaponAsync(userid);
            if (weapon == null)
            {
                return NotFound();
            }
            return Ok(weapon);
        }

        [HttpPut("{userid}")]
        [ProducesResponseType(typeof(WeaponDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<WeaponDto>> Put(int userid, [FromBody] WeaponDto weapon)
        {
            var result = await _weaponService.UpdatePlayerWeaponAsync(userid, weapon);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(weapon);
        }
    }
}
