﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace S3_DC_Backend.Presentation.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{userid}")]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<UserDto>> GetAsync(int userid)
        {
            var user = await _userService.GetById(userid);
            if (user == null)
            {
                return NoContent();
            }
            return Ok(user);
        }

        [HttpPut]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<UserDto>> PutAsync([FromBody] UserDto user)
        {
            var result = await _userService.UpdateUser(user);
            if (result == null)
            {
                return NoContent();
            }
            return Ok(result);
        }
    }
}
