﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace S3_DC_Backend.Presentation.Controllers
{
    [Route("api/attack")]
    [ApiController]
    public class AttackController : ControllerBase
    {
        public readonly IAttackService _attackService;
        public AttackController(IAttackService attackService)
        {
            _attackService = attackService;
        }

        [HttpGet("{userid}")]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<UserDto>> GetAsync(int userid)
        {
            var user = await _attackService.AttackPlayerEnemyAsync(userid);
            if (user == null)
            {
                return NoContent();
            }
            return Ok(user);
        }
    }
}
