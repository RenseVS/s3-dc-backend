﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;
using S3_DC_Backend.Persistence.Database;
using S3_DC_Backend.Presentation;
using System.Net.Http.Json;
using Xunit;
using System.Net;

namespace S3_DC_Backend.IntegrationTests.ControllerTests
{
    public class AttackControllerTests : IntegrationTestBase
    {
        public AttackControllerTests(PresentationWebAppFactory<Startup> factory)
            : base(factory)
        { }

        private async void SeedUserData(IAppDbContext dbContext, User user)
        {
            dbContext.Users.Add(user);
            dbContext.WeaponRarities.Add(new WeaponRarity
            {
                Id = 1,
                AppearChance = 100,
                DamageModifier = 1,
                SpeedModifier = 1,
                Rarity = "rare",
                RarityColor = "red"
            });
            dbContext.EnemyRarities.Add(new EnemyRarity
            {
                Id = 1,
                AppearChance = 100,
                DamageModifier = 1,
                SpeedModifier = 1,
                Rarity = "rare",
                RarityColor = "red",
                HealthModifier = 1
            });
            await dbContext.SaveChangesAsync();
            dbContext.Weapons.Add(new Weapon
            {
                UserId = user.Id,
                BaseDamage = 1,
                BaseSpeed = 1,
                Name = "stick",
                WeaponIconPath = "path",
                WeaponRarityId = 1
            });
            dbContext.Enemies.Add(new Enemy
            {
                UserId = user.Id,
                BaseDamage = 1,
                BaseSpeed = 1,
                EnemyIconPath = "path",
                EnemyRarityId = 1,
                Health = 1,
                MaxHealth = 1,
                Name = "enemy",
                WeaponIconPath = "path"
            });
            await dbContext.SaveChangesAsync();
        }

        [Fact]
        public async Task Post_Attack_ValidRequest_JsonResponseAndOK()
        {
            var dbContext = GetDbContext();
            var client = CreateHttpClient();
            await ClearDatabaseAsync();
            User user = new User()
            {
                Health = 1,
                Id = 1,
                MaxHealth = 1,
                Name = "Username",
            };
            SeedUserData(dbContext, user);

            var response = await client.GetAsync("/api/attack/1");
            var responseBody = await response.Content.ReadAsStreamAsync();
            var assignment = await JsonSerializer.DeserializeAsync<UserDto>(responseBody);

            Assert.NotNull(assignment);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotNull(assignment.Weapon);
            Assert.NotNull(assignment.Weapon.WeaponRarity);
            Assert.NotNull(assignment.Enemy);
            Assert.NotNull(assignment.Enemy.EnemyRarity);
        }
        [Fact]
        public async Task Post_Attack_inValidRequest_NoContent()
        {
            var client = CreateHttpClient();
            await ClearDatabaseAsync();

            var response = await client.GetAsync("/api/attack/1");

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}
