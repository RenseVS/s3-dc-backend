﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Presentation;
using Xunit;

namespace S3_DC_Backend.IntegrationTests
{
    public class IntegrationTestBase : IClassFixture<PresentationWebAppFactory<Startup>>, IDisposable
    {
        readonly PresentationWebAppFactory<Startup> _factory;
        readonly IServiceScope _serviceScope;

        protected IntegrationTestBase(PresentationWebAppFactory<Startup> factory)
        {
            _factory = factory;

            var scopeFactory = _factory.Services.GetService<IServiceScopeFactory>();
            _serviceScope = scopeFactory?.CreateScope();
        }

        protected IAppDbContext GetDbContext()
        {
            var dbContext = _serviceScope.ServiceProvider.GetService<IAppDbContext>();

            return dbContext;
        }

        protected async Task ClearDatabaseAsync()
        {
            var dbContext = GetDbContext() as DbContext;
            if (dbContext != null)
            {
                await dbContext.Database.EnsureDeletedAsync();
            }
        }


        protected HttpClient CreateHttpClient()
        {
            var client = _factory.CreateClient();

            return client;
        }


        public void Dispose()
        {
            _serviceScope?.Dispose();
        }
    }
}
