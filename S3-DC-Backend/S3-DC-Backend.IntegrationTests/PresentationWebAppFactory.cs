﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Persistence.Database;

namespace S3_DC_Backend.IntegrationTests
{
    public class PresentationWebAppFactory<TEntryPoint>
            : WebApplicationFactory<TEntryPoint>
            where TEntryPoint : class
    {
        protected override IHostBuilder CreateHostBuilder() =>
            base.CreateHostBuilder().UseEnvironment("Testing");

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType == typeof(IAppDbContext));

                services.Remove(descriptor);

                services.AddDbContext<IAppDbContext, AppDbContext>(options =>
                    options.UseInMemoryDatabase(databaseName: "S3-DC-Backend"));
            });
        }
    }

}
