﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace S3_DC_Backend.Persistence.Migrations
{
    public partial class MaxHealth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Health",
                table: "EnemyTemplates");

            migrationBuilder.AlterColumn<int>(
                name: "Health",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext");

            migrationBuilder.AddColumn<int>(
                name: "MaxHealth",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxHealth",
                table: "EnemyTemplates",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxHealth",
                table: "Enemies",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxHealth",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "MaxHealth",
                table: "EnemyTemplates");

            migrationBuilder.DropColumn(
                name: "MaxHealth",
                table: "Enemies");

            migrationBuilder.AlterColumn<string>(
                name: "Health",
                table: "Users",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Health",
                table: "EnemyTemplates",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
