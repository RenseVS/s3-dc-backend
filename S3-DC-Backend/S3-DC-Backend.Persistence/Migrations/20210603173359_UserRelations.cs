﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace S3_DC_Backend.Persistence.Migrations
{
    public partial class UserRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Weapons_WeaponId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_WeaponId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "WeaponId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Weapons",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Health",
                table: "Users",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Enemies",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Weapons_UserId",
                table: "Weapons",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enemies_UserId",
                table: "Enemies",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Enemies_Users_UserId",
                table: "Enemies",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Weapons_Users_UserId",
                table: "Weapons",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enemies_Users_UserId",
                table: "Enemies");

            migrationBuilder.DropForeignKey(
                name: "FK_Weapons_Users_UserId",
                table: "Weapons");

            migrationBuilder.DropIndex(
                name: "IX_Weapons_UserId",
                table: "Weapons");

            migrationBuilder.DropIndex(
                name: "IX_Enemies_UserId",
                table: "Enemies");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Weapons");

            migrationBuilder.DropColumn(
                name: "Health",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Enemies");

            migrationBuilder.AddColumn<int>(
                name: "WeaponId",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Users_WeaponId",
                table: "Users",
                column: "WeaponId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Weapons_WeaponId",
                table: "Users",
                column: "WeaponId",
                principalTable: "Weapons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
