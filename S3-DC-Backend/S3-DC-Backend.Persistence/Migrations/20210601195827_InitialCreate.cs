﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace S3_DC_Backend.Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnemyRarities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Rarity = table.Column<string>(nullable: false),
                    RarityColor = table.Column<string>(nullable: false),
                    DamageModifier = table.Column<int>(nullable: false),
                    SpeedModifier = table.Column<int>(nullable: false),
                    AppearChance = table.Column<int>(nullable: false),
                    HealthModifier = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnemyRarities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnemyTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Health = table.Column<int>(nullable: false),
                    EnemyIconPath = table.Column<string>(nullable: false),
                    BaseSpeed = table.Column<int>(nullable: false),
                    BaseDamage = table.Column<int>(nullable: false),
                    WeaponIconPath = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnemyTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WeaponRarities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Rarity = table.Column<string>(nullable: false),
                    RarityColor = table.Column<string>(nullable: false),
                    DamageModifier = table.Column<int>(nullable: false),
                    SpeedModifier = table.Column<int>(nullable: false),
                    AppearChance = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeaponRarities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WeaponTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    WeaponIconPath = table.Column<string>(nullable: false),
                    BaseSpeed = table.Column<int>(nullable: false),
                    BaseDamage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeaponTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enemies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EnemyRarityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Health = table.Column<int>(nullable: false),
                    EnemyIconPath = table.Column<string>(nullable: false),
                    BaseSpeed = table.Column<int>(nullable: false),
                    BaseDamage = table.Column<int>(nullable: false),
                    WeaponIconPath = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enemies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enemies_EnemyRarities_EnemyRarityId",
                        column: x => x.EnemyRarityId,
                        principalTable: "EnemyRarities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Weapons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    WeaponRarityId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    WeaponIconPath = table.Column<string>(nullable: false),
                    BaseSpeed = table.Column<int>(nullable: false),
                    BaseDamage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weapons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Weapons_WeaponRarities_WeaponRarityId",
                        column: x => x.WeaponRarityId,
                        principalTable: "WeaponRarities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    WeaponId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Weapons_WeaponId",
                        column: x => x.WeaponId,
                        principalTable: "Weapons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enemies_EnemyRarityId",
                table: "Enemies",
                column: "EnemyRarityId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_WeaponId",
                table: "Users",
                column: "WeaponId");

            migrationBuilder.CreateIndex(
                name: "IX_Weapons_WeaponRarityId",
                table: "Weapons",
                column: "WeaponRarityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enemies");

            migrationBuilder.DropTable(
                name: "EnemyTemplates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "WeaponTemplates");

            migrationBuilder.DropTable(
                name: "EnemyRarities");

            migrationBuilder.DropTable(
                name: "Weapons");

            migrationBuilder.DropTable(
                name: "WeaponRarities");
        }
    }
}
