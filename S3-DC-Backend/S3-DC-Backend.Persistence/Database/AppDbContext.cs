﻿using System;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Domain.Classes;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Persistence.Database
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext()
            : base()
        {

        }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }

        public DbSet<Weapon> Weapons { get; set; }

        public DbSet<WeaponTemplate> WeaponTemplates { get; set; }

        public DbSet<Enemy> Enemies { get; set; }

        public DbSet<EnemyTemplate> EnemyTemplates { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<WeaponRarity> WeaponRarities { get; set; }

        public DbSet<EnemyRarity> EnemyRarities { get; set; }
    }
}
