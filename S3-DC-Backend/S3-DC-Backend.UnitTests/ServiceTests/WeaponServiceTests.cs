﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Dtos.DescriptionDtos;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Domain.Classes;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;
using S3_DC_Backend.Persistence.Database;
using Xunit;

namespace S3_DC_Backend.UnitTests.ServiceTests
{
    public class WeaponServiceTests
    {
        readonly DbContextOptions<AppDbContext> _dbContextOptions;

        public WeaponServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "s3_project_WeaponServiceTests")
                .Options;
        }

        [Theory]
        [InlineData(1, 1, "stick", "path")]
        public async Task GetPLayerWeaponAsync_ValidInput_ReturnsCorrectData(int basespeed, int basedamage, string name, string path)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            var rarity = new WeaponRarity
            {
                AppearChance = 100,
                SpeedModifier = 1,
                DamageModifier = 1,
                Rarity = "rare",
                RarityColor = "colored"
            };
            dbContext.WeaponRarities.Add(rarity);
            var user = new User
            {
                Health = 1,
                MaxHealth = 1,
                Name = "name",
                Id = 1
            };
            dbContext.Users.Add(user);
            await dbContext.SaveChangesAsync();
            var data = new Weapon
            {
                UserId = 1,
                BaseDamage = basedamage,
                BaseSpeed = basespeed,
                Name = name,
                WeaponIconPath = path,
                WeaponRarityId = rarity.Id
            };
            dbContext.Weapons.Add(data);
            await dbContext.SaveChangesAsync();

            var service = new WeaponService(dbContext);
            var result = await service.GetPlayerWeaponAsync(1);

            Assert.Equal(basedamage, result.BaseDamage);
            Assert.Equal(basespeed, result.BaseSpeed);
            Assert.Equal(name, result.Name);
            Assert.Equal(path, result.WeaponIconPath);
            Assert.Equal(rarity.Id, result.WeaponRarity.Id);
        }

        [Fact]
        public async Task GetPLayerWeaponAsync_InValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            var service = new WeaponService(dbContext);
            var result = await service.GetPlayerWeaponAsync(int.MaxValue);
            Assert.Null(result);
        }

        [Theory]
        [InlineData(1, 1, "stick", "path", 1)]
        public async Task UpdatePLayerWeaponAsync_ValidInput_ReturnsCorrectData(int basespeed, int basedamage, string name, string path, int rarityid)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            var data = new WeaponDto
            {
                BaseDamage = basedamage,
                BaseSpeed = basespeed,
                Name = name,
                WeaponIconPath = path,
                WeaponRarity = new WeaponRarityDescriptionDto()
                {
                    Id = rarityid
                }
            };
            UpdatePLayerWeaponAsyncSeed(dbContext);

            var service = new WeaponService(dbContext);
            var result = await service.UpdatePlayerWeaponAsync(1, data);

            Assert.Equal(basedamage, result.BaseDamage);
            Assert.Equal(basespeed, result.BaseSpeed);
            Assert.Equal(name, result.Name);
            Assert.Equal(path, result.WeaponIconPath);
            Assert.Equal(rarityid, result.WeaponRarity.Id);
        }

        private async void UpdatePLayerWeaponAsyncSeed(AppDbContext dbContext)
        {
            var rarity = new WeaponRarity
            {
                AppearChance = 50,
                SpeedModifier = 1,
                DamageModifier = 1,
                Rarity = "rare",
                RarityColor = "colored",
                Id = 1
            };
            var rarity2 = new WeaponRarity
            {
                AppearChance = 50,
                SpeedModifier = 1,
                DamageModifier = 1,
                Rarity = "rare",
                RarityColor = "colored",
                Id = 2
            };
            dbContext.WeaponRarities.Add(rarity);
            dbContext.WeaponRarities.Add(rarity2);
            var user = new User
            {
                Health = 1,
                MaxHealth = 1,
                Name = "name",
                Id = 1
            };
            dbContext.Users.Add(user);
            await dbContext.SaveChangesAsync();
            var weapon = new Weapon
            {
                UserId = 1,
                BaseDamage = 0,
                BaseSpeed = 0,
                Name = "empty",
                WeaponIconPath = "Empty",
                WeaponRarityId = rarity2.Id
            };
            dbContext.Weapons.Add(weapon);
            await dbContext.SaveChangesAsync();
        }

        [Theory]
        [InlineData(1, 1, "stick", "path", 1)]
        public async Task UpdatePLayerWeaponAsync_InValidInput_ReturnsCorrectData(int basespeed, int basedamage, string name, string path, int rarityid)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            var data = new WeaponDto
            {
                BaseDamage = basedamage,
                BaseSpeed = basespeed,
                Name = name,
                WeaponIconPath = path,
                WeaponRarity = new WeaponRarityDescriptionDto()
                {
                    Id = rarityid
                }
            };

            var service = new WeaponService(dbContext);
            var result = await service.UpdatePlayerWeaponAsync(1, data);

            Assert.Null(result);
        }
    }
}
