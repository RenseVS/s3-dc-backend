﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Database;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Domain.Classes;
using S3_DC_Backend.Persistence.Database;
using Xunit;

namespace S3_DC_Backend.UnitTests.ServiceTests
{
    public class WeaponTemplateServiceTests
    {
        readonly DbContextOptions<AppDbContext> _dbContextOptions;

        public WeaponTemplateServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "s3_project_WeaponTemplateServiceTests")
                .Options;
        }

        [Theory]
        [InlineData(1, 1, "stick", "path", 4)]
        public async Task GetAllWeaponTemplates_ValidInput_ReturnsCorrectData(int basespeed, int basedamage, string name, string path, int amount)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            for (int i = 0; i < amount; i++)
            {
                var data = new WeaponTemplate
                {
                    BaseDamage = basedamage,
                    BaseSpeed = basespeed,
                    Name = name,
                    WeaponIconPath = path
                };
                dbContext.WeaponTemplates.Add(data);
            }
            await dbContext.SaveChangesAsync();

            var service = new WeaponTemplateService(dbContext);
            var result = await service.GetAllAsync();
            var resultList = result.ToList();

            Assert.Equal(amount, resultList.Count());
            Assert.Equal(basespeed, resultList[0].BaseSpeed);
            Assert.Equal(basedamage, resultList[0].BaseDamage);
            Assert.Equal(name, resultList[0].Name);
            Assert.Equal(path, resultList[0].WeaponIconPath);
        }

        [Fact]
        public async Task GetPLayerWeaponAsync_InValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            var service = new WeaponTemplateService(dbContext);
            var result = await service.GetAllAsync();
            Assert.Empty(result);
        }
    }
}
