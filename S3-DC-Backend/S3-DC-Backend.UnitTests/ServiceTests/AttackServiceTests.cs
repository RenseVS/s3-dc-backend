﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;
using S3_DC_Backend.Persistence.Database;
using Xunit;

namespace S3_DC_Backend.UnitTests.ServiceTests
{
    public class AttackServiceTests
    {
        readonly DbContextOptions<AppDbContext> _dbContextOptions;

        public AttackServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "s3_project_AttackService")
                .Options;
        }
        private async void SeedUserData(AppDbContext dbContext, User user, int enemyhealth, int playerspeed, int enemyspeed)
        {
            dbContext.Users.Add(user);
            dbContext.WeaponRarities.Add(new WeaponRarity
            {
                Id = 1,
                AppearChance = 100,
                DamageModifier = 1,
                SpeedModifier = 1,
                Rarity = "rare",
                RarityColor = "red"
            });
            dbContext.EnemyRarities.Add(new EnemyRarity
            {
                Id = 1,
                AppearChance = 100,
                DamageModifier = 1,
                SpeedModifier = 1,
                Rarity = "rare",
                RarityColor = "red",
                HealthModifier = 1
            });
            await dbContext.SaveChangesAsync();
            dbContext.Weapons.Add(new Weapon
            {
                UserId = user.Id,
                BaseDamage = 1,
                BaseSpeed = playerspeed,
                Name = "stick",
                WeaponIconPath = "path",
                WeaponRarityId = 1
            });
            dbContext.Enemies.Add(new Enemy
            {
                UserId = user.Id,
                BaseDamage = 1,
                BaseSpeed = enemyspeed,
                EnemyIconPath = "path",
                EnemyRarityId = 1,
                Health = enemyhealth,
                MaxHealth = enemyhealth,
                Name = "enemy",
                WeaponIconPath = "path"
            });
            await dbContext.SaveChangesAsync();
        }

        [Theory]
        [InlineData(1, 1, 1, 1)]
        [InlineData(1, 1, 1, 2)]
        [InlineData(1, 1, 2, 1)]
        public async Task UpdatePLayerWeaponAsync_ValidInputKills_ReturnsCorrectData(int playerhealth, int enemyhealth, int playerspeed, int enemyspeed)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            SeedUserData(dbContext, new User
            {
                Health = playerhealth,
                Id = 1,
                MaxHealth = playerhealth,
                Name = "username"
            }, enemyhealth, playerspeed, enemyspeed);

            var service = new AttackService(new UserService(dbContext), new EnemyService(dbContext));
            var result = await service.AttackPlayerEnemyAsync(1);

            if(playerspeed < enemyspeed)
            {
                Assert.Equal(enemyhealth, result.Enemy.Health);
                Assert.Equal(0, result.Health);
            }
            else
            {
                Assert.Equal(0, result.Enemy.Health);
                Assert.Equal(playerhealth, result.Health);
            }
            Assert.NotNull(result.Weapon);
            Assert.NotNull(result.Enemy);
        }

        [Theory]
        [InlineData(2, 2, 1, 1)]
        [InlineData(2, 2, 2, 1)]
        public async Task UpdatePLayerWeaponAsync_ValidInputDoesntKill_ReturnsCorrectData(int playerhealth, int enemyhealth, int playerspeed, int enemyspeed)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            SeedUserData(dbContext, new User
            {
                Health = playerhealth,
                Id = 1,
                MaxHealth = playerhealth,
                Name = "username"
            }, enemyhealth, playerspeed, enemyspeed);

            var service = new AttackService(new UserService(dbContext), new EnemyService(dbContext));
            var result = await service.AttackPlayerEnemyAsync(1);

            Assert.Equal(playerhealth - 1, result.Health);
            Assert.Equal(enemyhealth - 1, result.Enemy.Health);
            Assert.NotNull(result.Weapon);
            Assert.NotNull(result.Enemy);
        }

        [Fact]
        public async Task UpdatePLayerWeaponAsync_InValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();

            var service = new AttackService(new UserService(dbContext), new EnemyService(dbContext));
            var result = await service.AttackPlayerEnemyAsync(1);

            Assert.Null(result);
        }
    }
}
