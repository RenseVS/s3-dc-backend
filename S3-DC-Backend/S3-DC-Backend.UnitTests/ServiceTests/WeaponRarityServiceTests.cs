﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Dtos;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;
using S3_DC_Backend.Persistence.Database;
using Xunit;

namespace S3_DC_Backend.UnitTests.ServiceTests
{
    public class WeaponRarityServiceTests
    {
        readonly DbContextOptions<AppDbContext> _dbContextOptions;

        public WeaponRarityServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "s3_project_WeaponRarityService")
                .Options;
        }

        [Fact]
        public async Task GetWeaponRarityAsync_ValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            dbContext.WeaponRarities.Add(new WeaponRarity
            {
                SpeedModifier = 1,
                AppearChance = 50,
                DamageModifier = 1,
                Rarity = "rare",
                RarityColor = "red",
                Id = 1
            });
            dbContext.WeaponRarities.Add(new WeaponRarity
            {
                SpeedModifier = 1,
                AppearChance = 50,
                DamageModifier = 1,
                Rarity = "rare",
                RarityColor = "red",
                Id = 2
            });
            await dbContext.SaveChangesAsync();

            var service = new WeaponRarityService(dbContext);
            var result = await service.GetAllAsync();

            Assert.Equal(2, result.Count());
        }
        [Fact]
        public async Task GetWeaponRarityAsync_InValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();

            var service = new WeaponRarityService(dbContext);
            var result = await service.GetAllAsync();

            Assert.Empty(result);
        }
    }
}
