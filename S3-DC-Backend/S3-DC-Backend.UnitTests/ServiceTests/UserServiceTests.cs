﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S3_DC_Backend.Application.Services;
using S3_DC_Backend.Domain.Classes.GameObjects;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;
using S3_DC_Backend.Persistence.Database;
using Xunit;

namespace S3_DC_Backend.UnitTests.ServiceTests
{
    public class UserServiceTests
    {
        readonly DbContextOptions<AppDbContext> _dbContextOptions;

        public UserServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "s3_project_UserService")
                .Options;
        }
        private async void SeedUserData(AppDbContext dbContext, User user)
        {
            dbContext.Users.Add(user);
            await dbContext.SaveChangesAsync();
            dbContext.Weapons.Add(new Weapon
            {
                UserId = user.Id
            });
            dbContext.Enemies.Add(new Enemy
            {
                UserId = user.Id
            });
            await dbContext.SaveChangesAsync();
        }

        [Theory]
        [InlineData(1,1,1,"dave")]
        public async Task UpdatePLayerWeaponAsync_ValidInput_ReturnsCorrectData(int health, int id, int maxhealth, string name)
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();
            SeedUserData(dbContext , new User
            {
                Health = health,
                Id = id,
                MaxHealth = maxhealth,
                Name = name
            });

            var service = new UserService(dbContext);
            var result = await service.GetByIdWithWeaponAndEnemy(id);

            Assert.Equal(id, result.Id);
            Assert.Equal(health, result.Health);
            Assert.Equal(maxhealth, result.MaxHealth);
            Assert.Equal(name, result.Name);
            Assert.NotNull(result.Weapon);
            Assert.NotNull(result.Enemy);
        }
        [Fact]
        public async Task UpdatePLayerWeaponAsync_InValidInput_ReturnsCorrectData()
        {
            await using var dbContext = new AppDbContext(_dbContextOptions);
            await dbContext.Database.EnsureDeletedAsync();

            var service = new WeaponRarityService(dbContext);
            var result = await service.GetAllAsync();

            Assert.Empty(result);
        }
    }
}
