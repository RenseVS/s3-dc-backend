﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S3_DC_Backend.Domain.Classes
{
    public class WeaponTemplate
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string WeaponIconPath { get; set; }

        [Required]
        public int BaseSpeed { get; set; }

        [Required]
        public int BaseDamage { get; set; }
    }
}
