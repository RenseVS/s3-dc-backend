﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S3_DC_Backend.Domain.Classes.GameObjects
{
    public class User
    {
        public Weapon Weapon { get; set; }

        public Enemy Enemy { get; set; }

        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Health{ get; set; }

        [Required]
        public int MaxHealth { get; set; }
    }
}
