﻿using System;
using System.ComponentModel.DataAnnotations;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Domain.Classes.GameObjects
{
    public class Enemy
    {
        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        [Required]
        public int EnemyRarityId { get; set; }

        public EnemyRarity EnemyRarity { get; set; }

        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Health { get; set; }

        [Required]
        public int MaxHealth { get; set; }

        [Required]
        public string EnemyIconPath { get; set; }

        [Required]
        public int BaseSpeed { get; set; }

        [Required]
        public int BaseDamage { get; set; }

        [Required]
        public string WeaponIconPath { get; set; }
    }
}
