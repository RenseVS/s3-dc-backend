﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S3_DC_Backend.Domain.Classes.GameObjects.Rarity
{
    public class EnemyRarity : BaseRarity
    {
        public int Id { get; set; }

        [Required]
        public int HealthModifier { get; set; }
    }
}
