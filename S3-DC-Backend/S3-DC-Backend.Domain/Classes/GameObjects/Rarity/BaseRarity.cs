﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S3_DC_Backend.Domain.Classes.GameObjects.Rarity
{
    public class BaseRarity
    {
        [Required]
        public string Rarity { get; set; }

        [Required]
        public string RarityColor { get; set; }

        [Required]
        public int DamageModifier { get; set; }

        [Required]
        public int SpeedModifier { get; set; }

        [Required]
        public int AppearChance { get; set; }
    }
}
