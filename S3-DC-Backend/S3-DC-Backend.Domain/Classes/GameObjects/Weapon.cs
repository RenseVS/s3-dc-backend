﻿using System;
using System.ComponentModel.DataAnnotations;
using S3_DC_Backend.Domain.Classes.GameObjects.Rarity;

namespace S3_DC_Backend.Domain.Classes.GameObjects
{
    public class Weapon
    {
        [Required]
        public int WeaponRarityId { get; set; }

        public WeaponRarity WeaponRarity { get; set; }

        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string WeaponIconPath { get; set; }

        [Required]
        public int BaseSpeed { get; set; }

        [Required]
        public int BaseDamage { get; set; }
    }
}
