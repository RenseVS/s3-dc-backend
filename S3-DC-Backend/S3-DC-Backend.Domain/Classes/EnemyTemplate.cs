﻿using System;
using System.ComponentModel.DataAnnotations;

namespace S3_DC_Backend.Domain.Classes
{
    public class EnemyTemplate
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int MaxHealth { get; set; }

        [Required]
        public string EnemyIconPath { get; set; }

        [Required]
        public int BaseSpeed { get; set; }

        [Required]
        public int BaseDamage { get; set; }

        [Required]
        public string WeaponIconPath { get; set; }
    }
}
